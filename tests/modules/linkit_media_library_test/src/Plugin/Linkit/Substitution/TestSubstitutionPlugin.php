<?php

namespace Drupal\linkit_media_library_test\Plugin\Linkit\Substitution;

use Drupal\linkit\Plugin\Linkit\Substitution\Media;

/**
 * A test substitution plugin.
 *
 * @Substitution(
 *   id = "test_substitution_asset",
 *   label = @Translation("Test Substitution Plugin"),
 * )
 */
class TestSubstitutionPlugin extends Media {

}
